//
//  EditAlertViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/18/21.
//

import UIKit

class EditAlertViewController: UIViewController {

    @IBOutlet var alertView: UIView!
    @IBOutlet var termEditText: UITextField!
    @IBOutlet var definitionEditText: UITextField!
    //card from FlashCardSetDetailViewController
    var card: Flashcard = Flashcard()
    //use this later to do things to the flashcards potentially
    var parentVC: FlashCardSetDetailViewController?
    //Index Path
    var indexPath: Int?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setup()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        do {
            try context.save()
        }
        catch {
            print("error")
        }
    }
    
    func setup()
    {
        alertView.layer.cornerRadius = 8.0
        //set term/def
        termEditText.text = card.term
        definitionEditText.text = card.definition
        //make it so it shows this is editable
        termEditText.becomeFirstResponder()
    }
    
    @IBAction func deleteFlashcard(_ sender: Any) {
        self.dismiss(animated: false, completion: {
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            let card = self.parentVC?.cards[self.indexPath ?? 0]
            context.delete(card!)
            self.parentVC?.cards.remove(at: self.indexPath!)
            self.parentVC?.tableView.reloadData()
        })
    }
    
    @IBAction func doneEditing(_ sender: Any) {
        self.dismiss(animated: false, completion: {
            //do something / save the edits
            self.card.term = self.termEditText.text
            self.card.definition = self.definitionEditText.text
            self.parentVC?.tableView.reloadData()
        })
    }
    
}
