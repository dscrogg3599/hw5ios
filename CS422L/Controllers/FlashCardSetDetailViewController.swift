//
//  FlashCardSetDetailActivity.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/3/21.
//

import Foundation
import UIKit
import CoreData

class FlashCardSetDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate
{
    var cards: [Flashcard] = []
    @IBOutlet var buttonView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var deleteButton: UIButton!
    @IBOutlet var studyButton: UIButton!
    @IBOutlet var addButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        tableView.delegate = self
        tableView.dataSource = self
        let longPressedGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gestureRecognizer:)))
        longPressedGesture.minimumPressDuration = 0.5
        longPressedGesture.delegate = self
        longPressedGesture.delaysTouchesBegan = true
        tableView.addGestureRecognizer(longPressedGesture)
        makeItPretty()
    }
    
    func clearData() {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Flashcard")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try context.execute(deleteRequest)
        } catch let error as NSError {
            print(error)
        }
        UserDefaults.standard.set(false, forKey: "isDownloaded")
        print("Data cleared")
    }
    
    func loadData() {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Flashcard")
        do {
            let cards = try context.fetch(fetchRequest) as! [Flashcard]
            updateData(cards: cards)
        } catch {
            print(error)
        }
    }
    
    func updateData(cards: [Flashcard]) {
        self.cards = cards
        self.tableView.reloadData()
    }

    
    //adds card
    @IBAction func addCard(_ sender: Any) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let newCard = NSEntityDescription.insertNewObject(forEntityName: "Flashcard", into: context) as! Flashcard
        newCard.term = "Term \(cards.count + 1)"
        newCard.definition = "Definition \(cards.count + 1)"
        cards.append(newCard)
        do {
            try context.save()
        }
        catch {
            print("error")
        }
        tableView.reloadData()
    }
    
    @objc func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer) {
        if (gestureRecognizer.state != .began) {
                return
            }

        let p = gestureRecognizer.location(in: tableView)
        if let indexPath = tableView.indexPathForRow(at: p) {
            createCustomAlert(card: cards[indexPath.row], indexPath: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardCell", for: indexPath) as! FlashcardTableViewCell
        cell.flashcardLabel.text = cards[indexPath.row].term
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alert = UIAlertController(title: cards[indexPath.row].term, message: cards[indexPath.row].definition, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Edit", style: .default, handler: {_ in
            self.createCustomAlert(card: self.cards[indexPath.row], indexPath: indexPath)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    func createCustomAlert(card: Flashcard, indexPath: IndexPath)
    {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let alertVC = sb.instantiateViewController(identifier: "EditAlertViewController") as! EditAlertViewController
        alertVC.parentVC = self
        alertVC.card = card
        alertVC.indexPath = indexPath.row
        alertVC.modalPresentationStyle = .overCurrentContext
        self.present(alertVC, animated: false, completion: nil)
    }
    
    //just a function to make everything look nice
    func makeItPretty()
    {
        buttonView.layer.cornerRadius = 8.0
        buttonView.layer.borderColor = UIColor.purple.cgColor
        buttonView.layer.borderWidth = 2.0
        deleteButton.layer.cornerRadius = 8.0
        studyButton.layer.cornerRadius = 8.0
        addButton.layer.cornerRadius = 8.0
    }
}
